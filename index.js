
const { request } = require("express");
const express = require("express");
const { join } = require("path");


const path = require("path");
const { response } = require("express");

const app = express();

const port = 8000;

app.use(express.static(__dirname + "/views"));

//MIDDLEWARE 
app.use((request, response, next) =>{
    console.log(`Time is: ${new Date()}`);
    next()
})

app.use((request, response, next) =>{
    console.log(`Request URL is ${__dirname + request.url}`);
    next()
})

//MONOLITHIC EXPRESS - GET URL PAGE
app.get("/", (request, response) =>{
    response.sendFile(path.join(__dirname + "/views/example-mockup.html"))
})


app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})